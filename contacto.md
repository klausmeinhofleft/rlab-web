---
layout: home
title: Contacto
permalink: /contacto/
group: navigation
nav_text: "Contacto"
nav_order: 4
---

Los medios oficiales de comunicación son el email de contacto, el canal de irc,
las redes sociales y el canal de telegram.

# Email de contacto

Dirección: [contacto@rlab.be](mailto:contacto@rlab.be)

Llave PGP: [0xC9D519D74A18022B](https://keys.fedoraproject.org/pks/lookup?op=get&search=0xC9D519D74A18022B)

# Canal de IRC

Podés entrar a IRC por el [web chat](https://chat.rlab.be/), o conectarte al
servidor con los datos siguientes:

```
Server: irc.kernelpanic.com.ar
Onion: kpanicfz4v7lrsrv.onion
Puerto: 6697
SSL: sí
Canal: #rlyeh_hacklab
```

# Redes sociales

[Twitter](https://twitter.com/rlyehlab)

[Diaspora](https://diaspora.com.ar/people/646853b09a1d0135074400145e5c073c)

# Canal de Telegram

El canal de telegram es un anillo de confianza, vení al lab y te agregamos :)



